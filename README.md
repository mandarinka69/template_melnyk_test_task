1. install Java 8
2. git clone https://mandarinka69@bitbucket.org/mandarinka69/template_melnyk_test_task.git
3. Open template_melnyk_test_task
4. Open terminal
5. Command for run test "mvn clean test"
6. Generate report "mwn site"
7. Go to template_melnyk_test_task/target/site and open allure-maven-plagin.html in Chrome

8. If you use Jenkins, need add testng.xml and use command for run and generate report (5, 6)