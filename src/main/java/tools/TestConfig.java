package tools;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class TestConfig {


    public static final String BASE_URL = "https://www.google.com/";

    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    private final Integer IMPLICITY_WAIT = 20;

    @Parameters({"browser"})
    @BeforeTest
    public void setUp(@Optional("chrome") String browser, final ITestContext context) {

        if (browser.equals("chrome")) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");

            WebDriverManager.chromedriver().setup();
            driver.set(new ChromeDriver(options));
            getDriver().manage().window().setSize(getDimension("desktop"));

            getDriver().manage().timeouts().implicitlyWait(IMPLICITY_WAIT, TimeUnit.SECONDS);
            System.out.println("Starting " + driver.get().getClass().getSimpleName() + "..."
                    + WebDriverManager.chromedriver().getDownloadedVersion());
        }

        //driver.get().manage().window().maximize();
    }

    @AfterTest(alwaysRun = true)
    public void BrowserQuit() {
        System.out.println("Closing " + driver.get().getClass().getSimpleName() + "...");
        if (driver.get() != null) {
            driver.get().quit();
        }
    }

    public static Dimension getDimension(String dimension) {
        switch (dimension) {
            case "desktop":
                return new Dimension(1366, 768);
            case "mobile":
                return new Dimension(320, 568);
            default:
                return new Dimension(1366, 768);
        }
    }

    public static WebDriver getDriver() {
        return driver.get();
    }
}
