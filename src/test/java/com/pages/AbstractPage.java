package com.pages;

 import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static tools.TestConfig.getDriver;

public class AbstractPage {
    private static final long DEFAULT_TIMEOUT_TO_WAIT = 20;

    public AbstractPage() {
        PageFactory.initElements(getDriver(), this);
        waitUntilLoaded();
    }

    public void waitForVisibilityOf(WebElement element) {
        waitForVisibilityOf(element, DEFAULT_TIMEOUT_TO_WAIT);
    }

    public void waitForVisibilityOf(WebElement element, long timeoutInSeconds) {
        webDriverWait(element, timeoutInSeconds).until(ExpectedConditions.visibilityOf(element));
    }

    private static WebDriver getWebDriverFromSearchContext(SearchContext searchContext) {
        if (searchContext instanceof WebDriver) {
            return (WebDriver) searchContext;
        } else if (searchContext instanceof WrapsDriver) {
            return getWebDriverFromSearchContext(((WrapsDriver) searchContext).getWrappedDriver());
        } else {
            return searchContext instanceof WrapsElement ? getWebDriverFromSearchContext(((WrapsElement) searchContext).getWrappedElement()) : null;
        }
    }

    private WebDriverWait webDriverWait(WebElement element, long timeInSeconds) {
        WebDriverWait waiter = new WebDriverWait(getWebDriverFromSearchContext(element), timeInSeconds);
        waiter.ignoring(StaleElementReferenceException.class);
        return waiter;
    }

    protected void waitUntilLoaded(){

    }
}
