package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static tools.TestConfig.getDriver;

public class MainPage extends AbstractPage{

    @FindBy(css = "input[type=\"text\"]")
    private WebElement inputTextField;

    @FindBy(css = "input[type=\"submit\"]")
    private WebElement buttonSubmit;

    @FindBy(css = "#pnnext > span:nth-child(2)")
    public WebElement buttonGoToNextPage;

    @Step("Input \"{0}\"  in text field")
    public MainPage sendInputText(String text){
        waitForVisibilityOf(inputTextField);
        inputTextField.sendKeys(text);
        return this;
    }

    @Step("Press button search")
    public MainPage pressButtonSubmit(){
        buttonSubmit.click();
        return this;
    }

    @Step("Open  link number {0}")
    public WebElement openLinkResult(int resultNumber){
        waitUntilLoaded();
        return getDriver().findElement(By.xpath("(//*[@class='LC20lb'])[" + resultNumber + "]"));
    }


    public WebElement getSearchResult(int resultNumber) {
        return getDriver().findElement(By.cssSelector("#rso .srg .g"));
    }


    @Step("Go to next page")
    public MainPage gotoNextPage() {
        buttonGoToNextPage.click();
        return this;
    }

    public List<WebElement> urlsOnSearchResultPage() {
        return getDriver().findElements(By.cssSelector("[class=\"LC20lb\"]"));
    }

    public void checkTitleOnFirstLink(String textSearch, int searchResult,  String messageError) {
        sendInputText(textSearch)
                .pressButtonSubmit()
                .getSearchResult(searchResult)
                ;

        openLinkResult(searchResult).click();

         Assert.assertTrue(getDriver().getTitle().toLowerCase().contains(textSearch), messageError);
    }

    public void checkUrlsOnPagination(String textSearch, String textToFind, int paginationNumber, String messageError) {

        sendInputText(textSearch)
                .pressButtonSubmit();

        List<WebElement> foundUrl = new ArrayList<>();
        for (int i = 0; i < paginationNumber; i++) {
            gotoNextPage();
            urlsOnSearchResultPage();
            for (WebElement webElement : urlsOnSearchResultPage()) {

                if (webElement.getText().contains(textToFind)) {
                    foundUrl.add(webElement);
                }
            }
        }

        Assert.assertTrue(foundUrl.size() >= 1, messageError);
    }
}
