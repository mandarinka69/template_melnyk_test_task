package com.tests;

import org.openqa.selenium.JavascriptExecutor;
import ru.yandex.qatools.allure.annotations.Step;
import tools.TestConfig;

import java.util.concurrent.TimeUnit;

public class AbstractWebTest extends TestConfig {
    @Step("Open page {0}")
    public void openUrl(String url) {
        getDriver().get(url);
        waitUntilRequestsHaveFinished();
    }


    public void waitUntilRequestsHaveFinished() {
        getDriver().manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        ((JavascriptExecutor) getDriver()).executeAsyncScript(
                "var callback = arguments[arguments.length - 1]; var xhr = new XMLHttpRequest(); xhr.open('POST', '/Ajax_call', true); xhr.onreadystatechange = function() { if (xhr.readyState == 4) {callback(xhr.responseText);}}; xhr.send();");
    }
}
