package com.tests;

import com.pages.MainPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CheckTitleOnFirstFindPage extends AbstractWebTest {
    public MainPage mainPage;
    String textSearch = "automation";

    @BeforeClass
    public void before() {
        mainPage = new MainPage();
    }

    @Test(priority = 1)
    public void testCheckFirstResultInGoogleSearch(){
        openUrl(BASE_URL);

        mainPage.checkTitleOnFirstLink(textSearch, 1,"Text \""+textSearch+"\" not found in title");
    }
}
