package com.tests;

import com.pages.MainPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CheckDomainOnSearchPage extends AbstractWebTest {
    public MainPage mainPage;
    String textSearch = "automation";
    String domainSearch = "testautomationday.com";

    @BeforeClass
    public void before() {
        mainPage = new MainPage();
    }


    @Test(priority = 2)
    public void testFindCorrectResultInGoogleSearch(){
        openUrl(BASE_URL);

        mainPage.checkUrlsOnPagination(textSearch, domainSearch, 6, "Domain result \"" + domainSearch+ "\" not found");

    }


}
